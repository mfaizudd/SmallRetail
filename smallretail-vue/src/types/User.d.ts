export default interface User
{
  id: string;
  username: string;
  email: string;
  name: string;
  dateCreated: Date;
  dateUpdated: Date;
}
