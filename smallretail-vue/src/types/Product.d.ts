export default interface Product
{
  id: string;
  name: string;
  barcode: string;
  price: number;
  dateCreated: Date;
  dateUpdated: Date;
}
