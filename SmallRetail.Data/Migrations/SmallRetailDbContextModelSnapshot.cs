﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using SmallRetail.Data;

namespace SmallRetail.Data.Migrations
{
    [DbContext(typeof(SmallRetailDbContext))]
    partial class SmallRetailDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 63)
                .HasAnnotation("ProductVersion", "5.0.5")
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            modelBuilder.Entity("SmallRetail.Data.Models.Product", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid");

                    b.Property<string>("Barcode")
                        .HasColumnType("text");

                    b.Property<DateTime>("DateCreated")
                        .HasColumnType("timestamp without time zone");

                    b.Property<DateTime>("DateUpdated")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<decimal>("Price")
                        .HasColumnType("numeric");

                    b.HasKey("Id");

                    b.ToTable("Products");

                    b.HasData(
                        new
                        {
                            Id = new Guid("23b610d0-3dfc-411b-9760-c496fc186568"),
                            Barcode = "016343470",
                            DateCreated = new DateTime(2019, 6, 11, 1, 21, 21, 263, DateTimeKind.Local).AddTicks(8314),
                            DateUpdated = new DateTime(2019, 11, 24, 22, 4, 44, 637, DateTimeKind.Local).AddTicks(6381),
                            Name = "possimus",
                            Price = 6200m
                        },
                        new
                        {
                            Id = new Guid("e05d1bee-fd95-4fd8-8d60-5d27a7eca451"),
                            Barcode = "980273337",
                            DateCreated = new DateTime(2020, 7, 20, 7, 28, 49, 981, DateTimeKind.Local).AddTicks(1506),
                            DateUpdated = new DateTime(2020, 1, 30, 20, 43, 45, 520, DateTimeKind.Local).AddTicks(7011),
                            Name = "autem",
                            Price = 200m
                        },
                        new
                        {
                            Id = new Guid("ba7b0648-9227-4ebf-9e64-7c5a6f7bdac6"),
                            Barcode = "697819469",
                            DateCreated = new DateTime(2020, 11, 13, 1, 58, 45, 573, DateTimeKind.Local).AddTicks(9172),
                            DateUpdated = new DateTime(2020, 10, 24, 3, 0, 45, 165, DateTimeKind.Local).AddTicks(3315),
                            Name = "nam",
                            Price = 500m
                        },
                        new
                        {
                            Id = new Guid("2867ba66-b7c3-493f-b146-7d96d2a3459f"),
                            Barcode = "684942884",
                            DateCreated = new DateTime(2020, 12, 7, 13, 32, 33, 974, DateTimeKind.Local).AddTicks(6594),
                            DateUpdated = new DateTime(2019, 11, 4, 10, 29, 32, 764, DateTimeKind.Local).AddTicks(2113),
                            Name = "eius",
                            Price = 2600m
                        },
                        new
                        {
                            Id = new Guid("6757822f-52e0-4f11-b821-174e77546244"),
                            Barcode = "391276076",
                            DateCreated = new DateTime(2020, 11, 5, 22, 46, 19, 613, DateTimeKind.Local).AddTicks(9734),
                            DateUpdated = new DateTime(2020, 5, 13, 5, 43, 18, 657, DateTimeKind.Local).AddTicks(1986),
                            Name = "laudantium",
                            Price = 9700m
                        },
                        new
                        {
                            Id = new Guid("b7567fb0-ec4a-4852-8784-00d08baa44bb"),
                            Barcode = "621919046",
                            DateCreated = new DateTime(2019, 2, 21, 21, 49, 31, 878, DateTimeKind.Local).AddTicks(834),
                            DateUpdated = new DateTime(2019, 8, 28, 5, 52, 54, 570, DateTimeKind.Local).AddTicks(6343),
                            Name = "non",
                            Price = 9400m
                        },
                        new
                        {
                            Id = new Guid("00406db7-e859-4fd4-ba99-f75697a312fc"),
                            Barcode = "748274736",
                            DateCreated = new DateTime(2019, 4, 17, 2, 11, 56, 356, DateTimeKind.Local).AddTicks(8480),
                            DateUpdated = new DateTime(2019, 8, 17, 13, 15, 59, 927, DateTimeKind.Local).AddTicks(3556),
                            Name = "deleniti",
                            Price = 4500m
                        },
                        new
                        {
                            Id = new Guid("f5289b5d-ad7f-4158-ad48-9ee4c89cdd8f"),
                            Barcode = "415934795",
                            DateCreated = new DateTime(2020, 2, 3, 17, 16, 11, 534, DateTimeKind.Local).AddTicks(5705),
                            DateUpdated = new DateTime(2021, 5, 5, 18, 2, 41, 89, DateTimeKind.Local).AddTicks(9504),
                            Name = "sunt",
                            Price = 6800m
                        },
                        new
                        {
                            Id = new Guid("1a8a5572-2fc4-4fa5-a20d-dfd80cb44594"),
                            Barcode = "313642488",
                            DateCreated = new DateTime(2020, 3, 31, 19, 35, 10, 891, DateTimeKind.Local).AddTicks(5166),
                            DateUpdated = new DateTime(2020, 1, 19, 5, 27, 25, 366, DateTimeKind.Local).AddTicks(3445),
                            Name = "cupiditate",
                            Price = 9200m
                        },
                        new
                        {
                            Id = new Guid("e8289523-1704-4f85-a208-1cab83e29f8f"),
                            Barcode = "551076225",
                            DateCreated = new DateTime(2019, 7, 16, 18, 51, 12, 983, DateTimeKind.Local).AddTicks(3561),
                            DateUpdated = new DateTime(2021, 2, 12, 11, 50, 0, 241, DateTimeKind.Local).AddTicks(2862),
                            Name = "aut",
                            Price = 8800m
                        });
                });

            modelBuilder.Entity("SmallRetail.Data.Models.Transaction", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid");

                    b.Property<DateTime>("DateCreated")
                        .HasColumnType("timestamp without time zone");

                    b.Property<DateTime>("DateUpdated")
                        .HasColumnType("timestamp without time zone");

                    b.HasKey("Id");

                    b.ToTable("Transactions");
                });

            modelBuilder.Entity("SmallRetail.Data.Models.TransactionProduct", b =>
                {
                    b.Property<Guid>("TransactionId")
                        .HasColumnType("uuid");

                    b.Property<Guid>("ProductId")
                        .HasColumnType("uuid");

                    b.Property<int>("Quantity")
                        .HasColumnType("integer");

                    b.HasKey("TransactionId", "ProductId");

                    b.HasIndex("ProductId");

                    b.ToTable("TransactionProducts");
                });

            modelBuilder.Entity("SmallRetail.Data.Models.User", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid");

                    b.Property<DateTime>("DateCreated")
                        .HasColumnType("timestamp without time zone");

                    b.Property<DateTime>("DateUpdated")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("Name")
                        .HasColumnType("text");

                    b.Property<string>("Password")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("Type")
                        .IsRequired()
                        .ValueGeneratedOnAdd()
                        .HasColumnType("varchar(10)")
                        .HasDefaultValue("User");

                    b.Property<string>("Username")
                        .IsRequired()
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.HasIndex("Email")
                        .IsUnique();

                    b.HasIndex("Username")
                        .IsUnique();

                    b.ToTable("Users");
                });

            modelBuilder.Entity("SmallRetail.Data.Models.TransactionProduct", b =>
                {
                    b.HasOne("SmallRetail.Data.Models.Product", "Product")
                        .WithMany("TransactionProducts")
                        .HasForeignKey("ProductId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("SmallRetail.Data.Models.Transaction", "Transaction")
                        .WithMany("TransactionProducts")
                        .HasForeignKey("TransactionId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Product");

                    b.Navigation("Transaction");
                });

            modelBuilder.Entity("SmallRetail.Data.Models.Product", b =>
                {
                    b.Navigation("TransactionProducts");
                });

            modelBuilder.Entity("SmallRetail.Data.Models.Transaction", b =>
                {
                    b.Navigation("TransactionProducts");
                });
#pragma warning restore 612, 618
        }
    }
}
